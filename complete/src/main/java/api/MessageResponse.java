package api;

import java.io.Serializable;

/**
 * Created by krosullivan on 06/02/2017.
 */
public class MessageResponse implements Serializable {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setData(String message) {
        this.message = message;
    }

    public MessageResponse(String message) {
        this.message = message;
    }
}
